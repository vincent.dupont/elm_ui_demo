module Specificaton exposing (..)

import Element
    exposing
        ( Element
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , fillPortion
        , height
        , padding
        , rgb255
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font


main =
    Element.layout []
        layout


title : Element msg
title =
    column [ spacing 10, width (fillPortion 3), padding 10 ]
        [ el [ centerX, Font.size 30, Font.bold ] (text "Chipotle")
        , el [ centerX ] (text "COMPANY PROFILE EXCLUSIVELY FOR HCM DEVELOPMENT")
        , el [ centerX ] (text "AS OF: JUNE 23, 2022")
        ]


logo : Element msg
logo =
    row [ spacing 10, width (fillPortion 1) ]
        [ el [] (text "company logo")
        , el [] (text " | ")
        , el [] (text "Databook logo")
        ]


layout =
    column [ width fill ]
        [ row [ width fill, Background.color (rgb255 210 206 206) ]
            [ title, logo ]
        ]
