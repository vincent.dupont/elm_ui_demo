module SlideOne exposing (..)

import Element
    exposing
        ( Element
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , fillPortion
        , height
        , padding
        , rgb255
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font


main =
    Element.layout [ Background.color (rgb255 210 206 206) ]
        layout


layout =
    column [ width fill, height fill, spacing 20 ]
        [ el [ centerX, centerY, Font.size 30, Font.bold ] (text "1467 sample ppt render")
        , el [ centerX, centerY ] (text "Click to add subtitle")
        ]
