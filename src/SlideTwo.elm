module SlideTwo exposing (..)

import Element
    exposing
        ( Element
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , fillPortion
        , height
        , image
        , moveDown
        , moveRight
        , none
        , padding
        , px
        , rgb255
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font


main =
    Element.layout [ Background.color (rgb255 207 226 243) ]
        layout


layout =
    column [ width fill, height fill ]
        [ el [ width fill, height (px 120) ] (image [ height (px 100), padding 10 ] { src = "./salesforce.png", description = "Salesforce logo" })
        , row [ width fill, centerY ]
            [ el [ width (fillPortion 1) ]
                (image [ height (px 100) ] { src = "./bedbath-and-beyond-svg-wikimedia-commons.png", description = "Bed Bath and Beyond" })
            , column [ width (fillPortion 2), height fill, spacing 20 ]
                [ el [ centerX, centerY, Font.size 52, Font.bold ] (text "1467: phase 1")
                , el [ centerX, centerY, Font.size 28, Font.color (rgb255 89 89 89) ] (text "Dynamically generated title slide")
                ]
            , el [ width (fillPortion 1) ] none
            ]
        ]
